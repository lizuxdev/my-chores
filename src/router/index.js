import Vue from 'vue'
import Router from 'vue-router'
import Chores from '@/components/Chores'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Chores',
      component: Chores
    }
  ]
})
